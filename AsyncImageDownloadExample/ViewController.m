//
//  ViewController.m
//  AsyncImageDownloadExample
//
//  Created by Jason Lagaac on 19/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

#import "ViewController.h"
#import "ImageCollectionCellCollectionViewCell.h"

@interface ViewController ()

@property (nonatomic, strong) NSMutableArray *imageURLs;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageURLs = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view, typically from a nib.
    //self.imageView = [[AsyncImageView alloc] init];
    [self loadImages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadImages {
    NSURL *url = [[NSURL alloc] initWithString:@"http://apps.aim-data.com/data/abc/triplej/onair.xml"];
    NSXMLParser *xmlparser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    xmlparser.delegate = self;

    [self.imageURLs removeAllObjects];

    [xmlparser parse];
    [self.collectionView reloadData];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
                                        namespaceURI:(NSString *)namespaceURI
                                       qualifiedName:(NSString *)qName
                                          attributes:(NSDictionary *)attributeDict {
    
    if ([elementName  isEqualToString:@"playoutItem"]) {
        NSURL *url =  [NSURL URLWithString:[attributeDict objectForKey:@"imageUrl"]];
        [self.imageURLs addObject:url];
    }

}


- (IBAction)downloadImage:(id)sender {
    [self loadImages];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionCellCollectionViewCell *cell = [collectionView
                                                   dequeueReusableCellWithReuseIdentifier:@"ImageCell"
                                                   forIndexPath:indexPath];
    
    cell.imageView.imageURL = [self.imageURLs objectAtIndex:indexPath.row];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.imageURLs count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

@end
