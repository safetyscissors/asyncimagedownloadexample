//
//  AsyncImageView.m
//  AsyncImageDownloadExample
//
//  Created by Jason Lagaac on 19/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

#import "AsyncImageView.h"

@interface AsyncImageView()  <NSURLConnectionDataDelegate>

@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) NSURLConnection *urlConnection;

@property (nonatomic, strong) NSMutableData *urlData;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end


@implementation AsyncImageView

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        // Load the image view
        self.imageView = [[UIImageView alloc] init];
        
        // Load the frame
        CGRect currentFrame = self.frame;
        currentFrame.origin = CGPointMake(0, 0);
        self.imageView.frame = currentFrame;
        [self addSubview:self.imageView];
        
        // Load the activity indicator
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.activityIndicator.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        [self insertSubview:self.activityIndicator aboveSubview:self.imageView];
        
        [self.activityIndicator startAnimating];
    }
    
    return self;
}

- (void)setImageURL:(NSURL *)imageURL {
    if (imageURL != nil) {
        _imageURL = imageURL;
        NSURLRequest *request = [NSURLRequest requestWithURL:imageURL];
        
        if (self.urlConnection != nil) {
            [self.urlConnection cancel];
            self.urlConnection = nil;
            self.urlData = nil;
        }
        
        self.imageView.image = nil;
        self.activityIndicator.hidden = false;
        [self.activityIndicator startAnimating];
        
        self.urlData = [[NSMutableData alloc] init];
        self.urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        [self.urlConnection start];
    } else {
        _imageURL = nil;
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.urlData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    self.imageView.image = [UIImage imageWithData:self.urlData];
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = true;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Error: %@", error);
}

@end
