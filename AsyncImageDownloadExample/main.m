//
//  main.m
//  AsyncImageDownloadExample
//
//  Created by Jason Lagaac on 19/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
