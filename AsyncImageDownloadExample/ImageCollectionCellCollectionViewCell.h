//
//  ImageCollectionCellCollectionViewCell.h
//  AsyncImageDownloadExample
//
//  Created by Jason Lagaac on 19/01/2015.
//  Copyright (c) 2015 Jason Lagaac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface ImageCollectionCellCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet AsyncImageView *imageView;

@end
